using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseMenu : MonoBehaviour
{

    public static PauseMenu Instance {get;set;}
    [SerializeField] GameObject pauseMenuGO, confirmQuitGO;
    [SerializeField] FirstPersonController fpc;
    [HideInInspector] public bool inPause = false;
    [HideInInspector] public bool inConvo = false;
    float walkSpeed;
    float runSpeed;
    float timeSpeed;


    void Awake(){
        if(Instance == null){
            Instance = this;
        }
    }

    void Start(){
        walkSpeed = fpc.m_WalkSpeed;
        runSpeed = fpc.m_RunSpeed;
        timeSpeed = Time.timeScale;
    }

    void Update(){
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(!inConvo){
                if(!inPause){
                    PauseGame();
                } else {
                    ResumeGame();
                }
            }
        }
    }

    public void PauseGame(){
        inPause=true;
        Time.timeScale = 0;
        pauseMenuGO.SetActive(true);
        PausePlayer();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible=true;
    }

    public void ResumeGame(){
        inPause=false;
        Time.timeScale = timeSpeed;
        pauseMenuGO.SetActive(false);
        confirmQuitGO.SetActive(false);
        ResumePlayer();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void EndConvo(){
        
        ResumeGame();
        inConvo = false;
    }

    public void PausePlayer(){
        fpc.enabled = false;
        fpc.m_WalkSpeed=0f;
        fpc.m_RunSpeed=0f;
    }

    public void ResumePlayer(){
        fpc.enabled = true;
        fpc.m_WalkSpeed=walkSpeed;
        fpc.m_RunSpeed=runSpeed;
    }

    public void QuitGamePopup(){
        confirmQuitGO.SetActive(true);
    }

    public void CloseQuitGamePopup(){
        confirmQuitGO.SetActive(false);
    }

    public void QuitGame(){
        Application.Quit();
    }

}

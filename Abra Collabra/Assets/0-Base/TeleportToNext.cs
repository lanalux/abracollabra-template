using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportToNext : MonoBehaviour
{
    public static TeleportToNext Instance {get;set;}

    public static int currentScene = 0;

    List<int> roomsInGame = new List<int>();
    [HideInInspector] public List<int> roomsAdded = new List<int>();

    void Awake(){
        if(Instance==null){
            Instance = this;
        }
    }

    void Start(){
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
        for (int i=2; i<SceneManager.sceneCountInBuildSettings-1; i++){
            roomsInGame.Add(i);
        }
    }

    public void LoadNextScene(){
        

        if(roomsInGame.Count==0){
            SceneManager.LoadScene("Credits", LoadSceneMode.Additive);
        } else {
            int rand = Random.Range(0, roomsInGame.Count);
            int newScene = roomsInGame[rand];
            roomsAdded.Add(newScene);
            SceneManager.LoadScene(newScene, LoadSceneMode.Additive);
            roomsInGame.Remove(roomsInGame[rand]);
        }
        

        // currentScene++;
        // if(currentScene < SceneManager.sceneCountInBuildSettings - 1){
        //     SceneManager.LoadScene(currentScene, LoadSceneMode.Additive);
        // } else if (currentScene == SceneManager.sceneCountInBuildSettings - 1){
        //     SceneManager.LoadScene("Credits", LoadSceneMode.Additive);
        // }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DialogueEditor;

public class Raycast : MonoBehaviour
{
    float distance = 10.0f;
    [SerializeField] CursorControls cursorScript;

    void Update(){
        if(!PauseMenu.Instance.inConvo && !PauseMenu.Instance.inPause){
            RaycastHit hit;
            if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, distance)){
                GameObject target = hit.collider.gameObject;
                NPCConversation npc = target.GetComponent<NPCConversation>();

                IInteractable interactable = target.GetComponent<IInteractable>();

                if(npc!=null){
                    string npcName = "TALK";
                    // if(npcName!=null)
                    cursorScript.CursorWithText(npcName);
                    if(Input.GetMouseButtonDown(0)){
                        PauseMenu.Instance.PausePlayer();
                        PauseMenu.Instance.inConvo = true;
                        Cursor.lockState = CursorLockMode.None;
                        Cursor.visible=true;
                        cursorScript.HideCursor();
                        ConversationManager.Instance.StartConversation(npc);
                    }
                } else if (interactable != null){
                    
                    cursorScript.CursorWithText("INTERACT");
                    if (Input.GetMouseButtonDown(0)){
                        interactable.StartInteraction();
                    }
                    else if (Input.GetMouseButtonUp(0)){
                        interactable.EndInteraction();
                    }
                } else {
                    cursorScript.CursorNoText();
                }
            }
        }
    }
}

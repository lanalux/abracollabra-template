using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour, IInteractable
{
    [SerializeField] GameObject door;

    public void EndInteraction()
    {
    }

    public void StartInteraction()
    {
        door.SetActive(false);
    }
}

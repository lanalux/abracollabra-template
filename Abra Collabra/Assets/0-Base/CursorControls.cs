using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorControls : MonoBehaviour
{

   [SerializeField] Text cursorText;
   [SerializeField] Image cursorDot;

   public void HideCursor(){
       cursorText.text = "";
       cursorDot.enabled = false;
   }

    public void CursorNoText(){
       cursorText.text = "";
       cursorDot.enabled = true;

    }

    public void CursorWithText(string message){
       cursorText.text = message;
       cursorDot.enabled = true;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class MoveScene : MonoBehaviour
{
    float moveAmount = 21.75f;
    // PostProcessVolume post;
    [SerializeField] bool isCredits;



    void Awake()
    {
        
        if(isCredits){
            float moveTo = (TeleportToNext.Instance.roomsAdded.Count+2) * moveAmount;
            this.transform.position = new Vector3(0,0,moveTo);
        } else {
            if(TeleportToNext.Instance!=null){
                float moveTo = (TeleportToNext.Instance.roomsAdded.Count+1) * moveAmount;
                this.transform.position = new Vector3(0,0,moveTo);
            }
        }
        
    }

    // void Start(){
    //     post = FindObjectOfType<PostProcessVolume>();
        
    //     if(post!=null){
    //         Camera.main.GetComponent<PostProcessVolume>().profile = post.profile;
    //     }
    // }
    

}

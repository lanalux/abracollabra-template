using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddNextScene : MonoBehaviour
{
    void OnTriggerEnter(Collider col){
        if(col.gameObject.layer == LayerMask.NameToLayer("Player")){
            // Debug.Log("Entered");
            TeleportToNext.Instance.LoadNextScene();
            this.GetComponent<Collider>().enabled=false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SparkleParty.ScriptableObjects.Audio;
using SparkleParty.ScriptableObjects.Channels;

namespace AbraCollabra.LanaLux
{
    public class SoundExample : MonoBehaviour
    {
        [Header("audio Cues")]
        [SerializeField] AudioCue mySoundEffect;

        [Header("Audio Config")]
        [SerializeField] AudioConfig sfxAudioConfig;

        [Header("Channels")]
        [SerializeField] AudioCueEventChannel sfxAudioChannel;

        void OnTriggerEnter(Collider other){
            if(other.CompareTag("Player")){
                PlaySoundEffect();
            }
        }

        void PlaySoundEffect(){
            if(sfxAudioChannel == null || sfxAudioConfig == null || mySoundEffect == null){
                Debug.LogWarning("SoundTrigger :: Missing Audio References! Check your script!");
                return;
            }
            sfxAudioChannel.RequestAudio(mySoundEffect, sfxAudioConfig, transform.position);

        }

    }
}
